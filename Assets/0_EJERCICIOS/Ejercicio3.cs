using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* CREAR UN SCRIPT QUE AL PULSAR LA TECLA "T",
 * APLIQUE UNA FUERZA A UN CUBO */

public class Ejercicio3 : MonoBehaviour
{

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * 10);
        }
    }
}
