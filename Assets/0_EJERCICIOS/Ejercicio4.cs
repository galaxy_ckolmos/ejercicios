using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* CREAR DOS VARIABLES FLOAT 
 * CON VALORES ALEATORIOS ENTRE 1 Y 5,
 * SUMARLAS, E IMPRIMIR EL RESULTADO
 */

public class Ejercicio4 : MonoBehaviour
{

    void Start()
    {
        float a = Random.Range(1F, 5F);
        float b = Random.Range(1F, 5F);

        float c = a + b;

        print(c);
    }

}
