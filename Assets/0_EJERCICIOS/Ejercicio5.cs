using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* HACER QUE UN OBJETO SE MUEVA
 * A LA DERECHA (eje X). AL PULSAR ESPACIO,
 * DEBE CAMBIAR DE SENTIDO
 */

public class Ejercicio5 : MonoBehaviour
{
    bool derecha = true; // Variable booleana

    void Update()
    {
        if (derecha) //Comprobamos si derecha vale true
        {
            transform.Translate(0.1F,0,0); // En caso afirmativo, desplazamos a la derecha
        }
        else // Si derecha vale false
        {
            transform.Translate(-0.1F, 0, 0); // Desplazamos a la izquierda
        }

        if (Input.GetKeyDown(KeyCode.Space)) // Si pulsamos espacio
        {
            derecha = !derecha; // Invertimos el valor de la variable booleana
        }
    }
}
