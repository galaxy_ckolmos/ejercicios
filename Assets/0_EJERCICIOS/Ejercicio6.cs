using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* CREAR UNA VARIABLE A LA QUE
 * SE LE A�ADA "A" CADA VEZ QUE
 * SE PULSA ESPACIO
 */
public class Ejercicio6 : MonoBehaviour
{
    string texto;

    void Start()
    {
        texto = "A";
    }

    void Update()
    {
        print(texto);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            texto += "A";
        }
    }
}
