using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* EJERCICIO 10
 * 
 * DESTRUIR LA CAMARA AL PULSAR 10 VECES ESPACIO
 * 
 */

public class Ejercicio10 : MonoBehaviour
{
    int contador = 0;

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            contador = contador + 1;

            if(contador == 10)
            {
                Destroy(Camera.main.gameObject);
            } 
        }
    }
}
