using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* EJERCICIO 11
 * 
 * APLICAR UNA FUERZA HACIA ARRIBA A UNA ESFERA AL PULSAR LA "F"
 * CUIDADO: La F en Unity hace foco en el panel donde est� el rat�n.
 * Mantener el rat�n en el panel Game
 * 
 */

public class Ejercicio11 : MonoBehaviour
{

    public Rigidbody esfera;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            esfera.AddForce(Vector3.up * 500);
            //esfera.AddForce(0,500,0); //Otra alternativa
        }
    }
}
