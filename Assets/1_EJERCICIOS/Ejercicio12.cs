using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* EJERCICIO 12
 * 
 * DESACTIVAR O ACTIVAR EL RENDERER DE UN CUBO AL PULSAR LA LETRA R
 * 
 */

public class Ejercicio12 : MonoBehaviour
{
    public Renderer renderer;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            renderer.enabled = !renderer.enabled;
        }
    }

    void MetodoAlternativo()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (renderer.enabled)
            {
                renderer.enabled = false;
            }
            else
            {
                renderer.enabled = true;
            }
        }
    }
}
