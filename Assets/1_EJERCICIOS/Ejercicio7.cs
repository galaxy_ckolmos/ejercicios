using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* EJERCICIO 7
 * CREAR UNA ESCENA EN LA QUE HAYA DOS ESFERAS CON RIGIDBODY SIN GRAVEDAD, UN SUELO Y 3 LUCES DE DIFERENTES COLORES
 * CREAR VARIABLES P�BLICAS GAMEOBJECT, RIGIDBODY Y 3 LIGHT
 * 
 * AL PULSAR ESPACIO, ACTIVAR LA GRAVEDAD DEL RIGIDBODY
 * */

public class Ejercicio7 : MonoBehaviour
{
    public GameObject gameObject;
    public Rigidbody rigidBody;
    public Light luz1;
    public Light luz2;
    public Light luz3;

    private void Start()
    {
        gameObject.name = "Cambio de nombre";
    }

    private void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            rigidBody.useGravity = true;
        }

        if (Input.GetKey(KeyCode.Alpha1) || Input.GetKey(KeyCode.Keypad1))
        {
            luz1.color = Color.red;
            //luz1.color = new Color(0.9F,0.4F,0.6f);
        }

        if (Input.GetKey(KeyCode.Alpha2) || Input.GetKey(KeyCode.Keypad2))
        {
            luz2.intensity = 10F;
        }

        if (Input.GetKey(KeyCode.Alpha3) || Input.GetKey(KeyCode.Keypad3))
        {
            luz3.range = 100;
        }
    }
}
