using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* EJERCICIO 8
 * 
 * SUMAR 1 AL FIELD OF VIEW DE LA C�MARA AL PULSADR LA C
 * 
 */

public class Ejercicio8 : MonoBehaviour
{
    public Camera camara;

    private void Update()
    {
        if (Input.GetKey(KeyCode.C))
        {
            camara.fieldOfView += 1f;
        }
    }
}
