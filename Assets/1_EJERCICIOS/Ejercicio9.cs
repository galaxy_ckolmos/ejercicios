using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* EJERCICIO 9
 * 
 * CAMBIAR EL COLOR DEL MATERIAL DE UN CUBO AL PULSAR LA M
 * 
 * 
 */

public class Ejercicio9 : MonoBehaviour
{
    public Renderer rendererCubo;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            rendererCubo.material.color = Color.red;
        }
    }
}
