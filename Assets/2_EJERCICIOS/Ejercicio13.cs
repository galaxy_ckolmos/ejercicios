using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* EJERCICIO 13
 * 
 * Crear una funci�n que imprima el nombre del game object
 * 
 */

public class Ejercicio13 : MonoBehaviour
{
    private void Start()
    {
        ImprimirNombre();
    }

    void ImprimirNombre()
    {
        print(gameObject.name);
    }
}
