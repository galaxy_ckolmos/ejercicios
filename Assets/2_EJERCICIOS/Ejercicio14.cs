using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* EJERCICIO 14
 * 
 * Hacer una funci�n que devuelva un n�mero aleatorio
 * 
 */

public class Ejercicio14 : MonoBehaviour
{
    private void Update()
    {
        //print(NumeroAleatorio());

        int aleatorio = NumeroAleatorio();
        print(aleatorio);
    }

    int NumeroAleatorio()
    {
        return Random.Range(1, 101);
    }
}
