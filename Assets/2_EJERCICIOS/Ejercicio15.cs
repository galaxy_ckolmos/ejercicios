using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* EJERCICIO 15
 * 
 * Hacer una funci�n multiplique 3 n�meros e imprima el resultado
 * 
 */

public class Ejercicio15 : MonoBehaviour
{
    private void Start()
    {
        Multiplicar3(5, 5, 5);
        Multiplicar3(2, 2, 2);
    }

    void Multiplicar3(float a, float b, float c)
    {
        float resultado = a * b * c;
        print(resultado);
    }
}
