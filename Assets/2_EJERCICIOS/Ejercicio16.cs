using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* EJERCICIO 16
 * 
 * Hacer una función que reciba dos strings y devuelva la concatenación
 * 
 */

public class Ejercicio16 : MonoBehaviour
{

    public string string1;
    public string string2;

    private void Start()
    {
        string final = Concatenar("Parry this ", "you filthy casual");
        print(final);
        string final2 = Concatenar("No subestimes ", "el poder de C#");
        print(final2);
        string final3 = Concatenar(string1,string2);
        print(final3);
    }

    string Concatenar(string a, string b)
    {
        return a + b;
    }
}
