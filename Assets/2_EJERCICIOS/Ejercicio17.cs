using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* EJERCICIO 17
 * 
 * Hacer una funci�n que reciba dos float de intensidad y rango y cambie
 * las propiedades de una luz
 * 
 */

public class Ejercicio17 : MonoBehaviour
{
    public Light luz;
    public float limite = 20;

    float i = 0;
    float r = 0;


    private void Update()
    {
        if (r < limite) {

            CambiarLuz(i, r);

            i += 0.01f;
            r += 0.1f;
        }
    }

    void CambiarLuz(float intensidad, float rango)
    {
        luz.intensity = intensidad;
        luz.range = rango;
    }
}
