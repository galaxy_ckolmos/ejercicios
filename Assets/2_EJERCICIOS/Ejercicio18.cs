using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/* EJERCICIO 18
 * 
 * Hacer una funci�n que reciba un GameObject y un float y destruya el objeto en ese tiempo
 * 
 */

public class Ejercicio18 : MonoBehaviour
{
    
    public GameObject o1;
    public GameObject o2;
    public GameObject o3;
    
    private void Start()
    {
        Destruir(o1, 1);
        Destruir(o2, 2);
        Destruir(o3, 3);
    }

    void Destruir(GameObject objeto, float tiempo)
    {
        Destroy(objeto, tiempo);
    }

}
