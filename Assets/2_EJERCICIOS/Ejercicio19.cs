using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* EJERCICIO 19
 * 
 * Hacer una funci�n que devuelva true si el n�mero recibido es par
 * 
 */

public class Ejercicio19 : MonoBehaviour
{

    private void Start()
    {
        int aleatorio = Random.Range(1, 101);
        print(aleatorio);

        if (EsPar(aleatorio))
        {
            print("Ha salido PAR");
        }
        else
        {
            print("Ha salido impar");
        }
    }

    bool EsPar(int numero)
    {
        if (numero % 2 == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
