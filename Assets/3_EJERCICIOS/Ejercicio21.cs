using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* EJERCICIO 20
 * 
 * Aumentar o disminuar el rango de una luz con los cursores
 */

public class Ejercicio21 : MonoBehaviour
{
    public Light luz;

    private void Update()
    {
        luz.range += Input.GetAxis("Vertical")*Time.deltaTime;
    }
}
