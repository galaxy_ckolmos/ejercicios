using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* EJERCICIO 22
 * 
 * Hacer una pelota con rebote infinito. Cada vez que rebote, otro objeto (que no sea la pelota ni el suelo)
 * debe cambiar de color.
 * 
 */

public class Ejercicio22 : MonoBehaviour
{
    public Renderer objeto;

    private void OnCollisionEnter(Collision collision)
    {
        Color c = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        objeto.material.color = c;
    }
}
