using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* EJERCICIO 23
 * 
 * Activar o desactivar un objeto usando un bot�n de UI
 * 
 */
public class Ejercicio23 : MonoBehaviour
{
    public GameObject objeto;

    public void BotonPulsado()
    {

        print(objeto.activeSelf);
        print(!objeto.activeSelf);

        objeto.SetActive(!objeto.activeSelf);
    }
}
