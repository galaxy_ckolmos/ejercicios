using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* EJERCICIO 24
 * 
 * Instanciar un cubo y darle una fuerza cada vez que hacemos click izquierdo
 * 
 */

public class Ejercicio24 : MonoBehaviour
{
    public GameObject prefab;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            GameObject instancia = Instantiate(prefab, Vector3.zero, Quaternion.identity);
            instancia.GetComponent<Rigidbody>().AddForce(Vector3.up * 500);
        }
    }
}
