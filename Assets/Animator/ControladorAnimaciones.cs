using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorAnimaciones : MonoBehaviour
{

    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float correr = Input.GetAxis("Vertical");

        animator.SetFloat("correr", correr);

        if (Input.GetButton("Fire1"))
        {
            animator.SetBool("picar", true);
        }
        if (Input.GetButtonUp("Fire1"))
        {
            animator.SetBool("picar", false);
        }
    }
}
