using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Agente : MonoBehaviour
{
    NavMeshAgent agente;
    public Material rojo;
    public Material azul;
    public GameObject disparoPrefab;
    
    GameObject objetivoSecundario;
    List<GameObject> objetivos = new List<GameObject>();
    int vida = 3;

    bool comprobandoEntorno = false;

    public void Inicializar(GameObject objetivo,string equipo)
    {
        agente = GetComponent<NavMeshAgent>();
        agente.SetDestination(objetivo.transform.position);
        objetivoSecundario = objetivo;
        tag = equipo;
        if (tag == "EquipoRojo")
        {
            GetComponent<Renderer>().material = rojo;
        }
        else
        {
            GetComponent<Renderer>().material = azul;
        }

        //StartCoroutine(Accion());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Untagged")
        {
            if (other.tag != tag)
            {
                objetivos.Add(other.gameObject);
                if (agente != null)
                {
                    if (!agente.isStopped)
                    {
                        if (!comprobandoEntorno)
                        {
                            agente.isStopped = true;
                            comprobandoEntorno = true;
                            StartCoroutine(Accion());
                            comprobandoEntorno = false;
                        }
                    }
                }
            }
        }

        if (other.gameObject.layer == LayerMask.NameToLayer("disparoRojo") && tag == "EquipoAzul")
        {
            vida--;
            Destroy(other.gameObject);
            if (vida == 0) Destroy(gameObject);
        }
        if (other.gameObject.layer == LayerMask.NameToLayer("disparoAzul") && tag == "EquipoRojo")
        {
            vida--;
            Destroy(other.gameObject);
            if (vida == 0) Destroy(gameObject);
        }

    }

    private void Update()
    {
        if(agente.isStopped && !comprobandoEntorno)
        {
            if (objetivoSecundario == null)
            {
                if (tag == "EquipoRojo")
                {
                    GameObject baseAzul = GameObject.Find("BaseAzul");
                    agente.SetDestination(baseAzul.transform.position);
                    agente.isStopped = false;
                }
                else
                {
                    GameObject baseRoja = GameObject.Find("BaseRoja");
                    agente.SetDestination(baseRoja.transform.position);
                    agente.isStopped = false;
                }
            }
        }
    }

    IEnumerator Accion()
    {
        while (objetivos.Count > 0)
        {

            if (objetivos[0] != null)
            {
                transform.LookAt(objetivos[0].transform);
                GameObject disparo = Instantiate(disparoPrefab, transform.position, transform.rotation);
                disparo.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * 100);
                Destroy(disparo, 2);
            }
            else
            {
                objetivos.RemoveAt(0);
            }
            yield return new WaitForSeconds(1);
        }

        if (objetivoSecundario != null)
        {
            agente.SetDestination(objetivoSecundario.transform.position);
            agente.isStopped = false;
        }
        else
        {
            if (tag == "EquipoRojo")
            {
                GameObject baseAzul = GameObject.Find("BaseAzul");
                agente.SetDestination(baseAzul.transform.position);
                agente.isStopped = false;
            }
            else
            {
                GameObject baseRoja = GameObject.Find("BaseRoja");
                agente.SetDestination(baseRoja.transform.position);
                agente.isStopped = false;
            }
        }

    }

}
