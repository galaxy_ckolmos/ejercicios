using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nucleo : MonoBehaviour
{
    public GameObject prefab;
    public Material rojo;
    public GameObject nucleoEnemigo;

    public GameObject[] objetivosSecundarios = new GameObject[3];

    void Start()
    {
        InvokeRepeating("Instanciar", 1, 3);
    }

    void Instanciar()
    {
        for(int i = 0; i < 3; i++)
        {
            GameObject minion = Instantiate(prefab, transform.GetChild(i).position, Quaternion.identity);

            GameObject pos=null;
            if (objetivosSecundarios[i] != null)
            {
                pos = objetivosSecundarios[i];
            }
            else
            {
                pos = nucleoEnemigo;
            }
            
            minion.GetComponent<Agente>().Inicializar(pos,tag);
            if (tag == "EquipoRojo")
            {
                minion.GetComponent<Renderer>().material = rojo;
                minion.tag = "EquipoRojo";
                minion.layer = LayerMask.NameToLayer("equipoRojo");
            }
            else
            {
                minion.tag = "EquipoAzul";
                minion.layer = LayerMask.NameToLayer("equipoAzul");
            }
        }
    }

    int vida = 3;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("disparoRojo") && tag == "EquipoAzul")
        {
            vida--;
            if (vida == 0) Destroy(gameObject);
            Destroy(other.gameObject);
        }
        if (other.gameObject.layer == LayerMask.NameToLayer("disparoAzul") && tag == "EquipoRojo")
        {
            vida--;
            if (vida == 0) Destroy(gameObject);
            Destroy(other.gameObject);
        }
    }
}

