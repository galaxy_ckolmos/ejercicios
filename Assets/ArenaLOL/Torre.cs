using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torre : MonoBehaviour
{
    int vida = 3;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("disparoRojo") && tag == "EquipoAzul")
        {
            vida--;
            if (vida == 0) Destroy(gameObject);
            Destroy(other.gameObject);
        }
        if (other.gameObject.layer == LayerMask.NameToLayer("disparoAzul") && tag == "EquipoRojo")
        {
            vida--;
            if (vida == 0) Destroy(gameObject);
            Destroy(other.gameObject);
        }
    }

}
