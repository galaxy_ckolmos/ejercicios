using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EjemplosArrays : MonoBehaviour
{

    int[] miArray = new int[10];

    public int[] arrayPublico;

    int[] miArray3 = new int[5];

    int[] miArray4 = new int[5];

    void Start()
    {

        EjemploForeach();
        
    }

    void Ejemplo1()
    {
        print(miArray[0]);
        print(miArray[4]);
        print(miArray[9]);

        miArray[0] = -5;
        miArray[4] = 100;
        miArray[9] = 200;

        print(miArray[0]);
        print(miArray[4]);
        print(miArray[9]);
    }

    void Ejemplo2()
    {
        //print(arrayPublico[2]);

        for (int i = 0; i < arrayPublico.Length; i++)
        {
            print(arrayPublico[i]);
        }
    }

    void Ejemplo3()
    {
        miArray3[0] = -5;
        miArray3[1] = 10;
        miArray3[2] = -115;
        miArray3[3] = -90;
        miArray3[4] = 85;

        for(int i=0; i < 5; i++)
        {
            print(miArray3[i]);
        }
    }

    void EjemploForeach()
    {
        miArray4[0] = -5;
        miArray4[1] = 10;
        miArray4[2] = -115;
        miArray4[3] = -90;
        miArray4[4] = 85;

        foreach (int numero in miArray4)
        {
            print(numero);
        }
    }
}
