using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GeneradorExcusas : MonoBehaviour
{

    public Text texto;

    public string[] excusas;

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            int excusaAleatoria = Random.Range(0, excusas.Length);
            texto.text = excusas[excusaAleatoria];
        }
    }
}
