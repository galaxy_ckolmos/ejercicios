using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LucesArrays : MonoBehaviour
{

    public Light[] luces;
    int posicionLuz = 0;
    bool encendiendo = true;
    bool navidadPar = true;
    
    void Start()
    {
        foreach(Light luz in luces)
        {
            luz.enabled = false;
        }

        //InvokeRepeating("CambiarLuz",0.25F,0.25F);
        InvokeRepeating("LucesNavidad", 0.25F, 0.25F);
        //InvokeRepeating("LucesSecuenciales", 0.25F, 0.25F);

    }

    void CambiarLuz()
    {
        if (encendiendo)
        {
            if (posicionLuz < luces.Length)
            {
                luces[posicionLuz].enabled = true;
                posicionLuz++;
            }
            else
            {
                encendiendo = false;
                posicionLuz--;
            }
        }
        else
        {
            if (posicionLuz >= 0)
            {
                luces[posicionLuz].enabled = false;
                posicionLuz--;
            }
            else
            {
                encendiendo = true;
                posicionLuz++;
            }
        }
        
    }

    void LucesNavidad()
    {

        for(int i = 0; i < luces.Length; i++)
        {
            if (i % 2==0) // PARA SABER SI ES PAR
            {
                if (navidadPar)
                {
                    luces[i].enabled = true;
                }
                else
                {
                    luces[i].enabled = false;
                }
                
            }
            else
            {
                if (navidadPar)
                {
                    luces[i].enabled = false;
                }
                else
                {
                    luces[i].enabled = true;
                }
            }
        }

        navidadPar = !navidadPar;
    }

    void LucesSecuenciales()
    {
        // %
        if (posicionLuz > 0)
        {
            luces[(posicionLuz-1) % luces.Length].enabled = false;
            luces[posicionLuz % luces.Length].enabled = true;
            posicionLuz++;
        }
        else
        {
            luces[0].enabled = true;
            posicionLuz++;
        }
    }

}
