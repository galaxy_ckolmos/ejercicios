using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/* A�adir este script a un objeto (por ejemplo un GameObject con un componente AudioSource) para que:
 * 
 * 1.- No se destruya al cambiar de escena
 * 2.- No se duplique al volver a la escena inicial
 * 3.- Destruya cualquier otra copia
 * 
 * */

public class AudioManagerSingleton : MonoBehaviour
{

    public static AudioManagerSingleton instancia;

    private void Awake()
    {
        if (instancia != null && instancia!=this)
        {
            DestroyImmediate(this);
        }
        else
        {
            instancia = this;
        }

        DontDestroyOnLoad(gameObject);
    }

}
