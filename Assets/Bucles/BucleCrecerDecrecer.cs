using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BucleCrecerDecrecer : MonoBehaviour
{
    GameObject[] array = new GameObject[10];

    bool crecer = true;

    private void Start()
    {
        for(int i=0; i<10; i++)
        {
            array[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
            array[i].transform.position = new Vector3(i, 0, 0);
            array[i].SetActive(false);
        }

        StartCoroutine(Corrutina());
    }

    IEnumerator Corrutina()
    {
        while (true)
        {
            for (int i = 0; i < 10; i++)
            {

                array[i].SetActive(true);


                yield return new WaitForSeconds(0.1F);
            }

            for (int i = 9; i >= 0; i--)
            {

                array[i].SetActive(false);


                yield return new WaitForSeconds(0.1F);
            }
        }
    }
}
