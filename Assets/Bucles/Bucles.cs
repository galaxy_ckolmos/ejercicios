using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bucles : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        BucleFor();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void BucleWhile()
    {
        int i = 0;
        while (i==10)
        {
            print("i vale " + i);
            i=i+3;
        }
    }

    void BucleFor()
    {
        for(int i=0; i<100; i++)
        {
            print("i vale " + i);
        }
    }
}
