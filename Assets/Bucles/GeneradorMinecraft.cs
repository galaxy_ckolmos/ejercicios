using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorMinecraft : MonoBehaviour
{
    public GameObject prefabSuelo;
    public GameObject prefabTierra;
    public GameObject prefabHierba;

    [Range(0,100)]
    public int porcentajeHierba;

    void Start()
    {
        for(int i = 0; i < 20; i++)
        {
            for (int j = 0; j < 20; j++)
            {
                for(int k = 0; k < 20; k++)
                {
                    int instanciar = Random.Range(0, 100);
                    if (instanciar < 90)
                    {

                        GameObject prefab = null;
                        if (k < 8)
                        {
                            prefab = prefabSuelo;
                        }
                        if (k >= 8 && k < 12)
                        {
                            int aleatorio = Random.Range(0, 2);
                            if (aleatorio == 0)
                            {
                                prefab = prefabSuelo;
                            }
                            else
                            {
                                prefab = prefabTierra;
                            }

                        }
                        if (k >= 12 && k < 18)
                        {
                            prefab = prefabTierra;
                        }
                        if (k == 18)
                        {
                            prefab = prefabHierba;
                        }
                        if (k == 19)
                        {
                            int aleatorio = Random.Range(0, 100);
                            if (aleatorio <= porcentajeHierba)
                            {
                                prefab = prefabHierba;
                            }
                            else
                            {
                                prefab = null;
                            }
                        }
                        if (prefab != null)
                        {
                            Instantiate(prefab, new Vector3(i, k, j), Quaternion.identity);
                        }
                        //yield return new WaitForSeconds(0.01F);
                    }
                }
            }
        }
    }
}
