using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piramide : MonoBehaviour
{
    int anchuraInicial=0;
    int anchuraFinal = 21;

    int profundidadInicial = 0;
    int profundidadFinal = 21;

    private void Start()
    {
        StartCoroutine(CrearHipercubo());
    }

    IEnumerator CrearCubo()
    {
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                for (int k = 0; k < 10; k++)
                {
                    int aleatorio = Random.Range(0, 100);

                    if (aleatorio < 90)
                    {
                        GameObject cubo = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        cubo.transform.position = new Vector3(i, k, j);
                    }
                    yield return new WaitForSeconds(0.01f);
                }
            }
        }
    }

    IEnumerator CrearPiramide()
    {
        for (int altura = 0; altura < 21; altura++)
        {
            for (int profundidad = profundidadInicial; profundidad < profundidadFinal; profundidad++)
            {
                for (int anchura = anchuraInicial; anchura < anchuraFinal; anchura++)
                {
                    Vector3 pos = new Vector3(anchura, altura, profundidad);
                    GameObject cubo = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    cubo.transform.position = pos;

                    yield return new WaitForSeconds(0.01f);
                }
            }

            anchuraInicial++;
            anchuraFinal--;

            profundidadInicial++;
            profundidadFinal--;

        }
    }

    IEnumerator CrearHipercubo()
    {
        for (int altura = 0; altura < 20; altura++)
        {
            for (int profundidad = 0; profundidad < 20; profundidad++)
            {
                for (int anchura = 0; anchura < 20; anchura++)
                {

                    bool comprobarAnchura = anchura == 0 || anchura == 19;
                    bool comprobarProfundidad = profundidad == 0 || profundidad == 19;

                    bool comprobarAltura = (altura==0 || altura==19) || (comprobarAnchura && comprobarProfundidad);
                    
                    if ( (comprobarAnchura || comprobarProfundidad) && comprobarAltura)
                    {
                        Vector3 pos = new Vector3(anchura, altura, profundidad);
                        GameObject cubo = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        cubo.transform.position = pos;

                        yield return new WaitForSeconds(0.01f);
                    }

                }
            }
        }
    }
}
