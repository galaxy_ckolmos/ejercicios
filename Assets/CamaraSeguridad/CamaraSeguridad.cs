using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraSeguridad : MonoBehaviour
{
    [Header("Parametros")]
    public float amplitud=20;
    public float retardoInicial = 0;

    float rotacionInicialY;

    bool iniciado = true;

    private void Start()
    {
        rotacionInicialY = transform.eulerAngles.y;
    }

    private void Update()
    {
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, rotacionInicialY+Mathf.Sin(Time.time)*amplitud, transform.eulerAngles.z);
    }
}
