using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class Persistencia
{

	// FUNCION GUARDAR
	public void Guardar(SaveData data)
	{
		BinaryFormatter bf = new BinaryFormatter();

		// File.Create crea un archivo en la ruta especificada
		FileStream file = File.Create(Application.persistentDataPath+ "/MySaveData.dat");

		bf.Serialize(file, data);
		file.Close();
		Debug.Log("Game data saved!");
	}

	// FUNCION CARGAR
	public SaveData Cargar()
	{
		if (File.Exists(Application.persistentDataPath + "/MySaveData.dat"))
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/MySaveData.dat", FileMode.Open);
			SaveData data = (SaveData)bf.Deserialize(file);
			file.Close();
			Debug.Log("Datos Cargados!");
			return data;
		}
		else
		{
			Debug.LogError("No hay datos!");
			return null;
		}
	}
}
