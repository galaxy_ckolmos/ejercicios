using System;

[Serializable]
public class SaveData
{
    public int savedInt;
    public float savedFloat;
    public bool savedBool;
}