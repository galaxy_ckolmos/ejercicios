using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager_CargarGuardar : MonoBehaviour
{

    public InputField inputInt;
    public InputField inputFloat;
    public Toggle toggle;

    public void BotonCargar()
    {
        Persistencia persistencia = new Persistencia();
        SaveData datos = persistencia.Cargar();

        if (datos != null)
        {
            inputInt.text = datos.savedInt.ToString();
            inputFloat.text = datos.savedFloat.ToString();
            toggle.enabled = datos.savedBool;
        }
    }


    public void BotonGuardar()
    {
        // La clase para guardar debe ser SERIALIZABLE (ver clase)
        SaveData data = new SaveData();
        data.savedInt = int.Parse(inputInt.text);
        data.savedFloat = float.Parse(inputFloat.text);
        data.savedBool = toggle.enabled;

        Persistencia persistencia = new Persistencia();
        persistencia.Guardar(data);
    }
}
