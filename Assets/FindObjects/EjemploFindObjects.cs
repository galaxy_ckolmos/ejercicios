using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EjemploFindObjects : MonoBehaviour
{
    private void Start()
    {
        //InvokeRepeating("OfType", 0.5F, 0.5F);
        //InvokeRepeating("WithTag", 0.5F, 0.5F);
        DesactivarGravedad();
    }


    void OfType()
    {
        Propulsar[] propulsores = FindObjectsOfType<Propulsar>();
        foreach (Propulsar propulsor in propulsores)
        {

            propulsor.AñadirFuerza();
        }

    }

    void WithTag()
    {
        GameObject[] propulsores = GameObject.FindGameObjectsWithTag("Propulsor");
        foreach (GameObject propulsor in propulsores)
        {

            propulsor.GetComponent<Propulsar>().AñadirFuerza();
        }

    }

    void DesactivarGravedad()
    {
        Rigidbody[] rbs = FindObjectsOfType<Rigidbody>();
        foreach (Rigidbody rb in rbs)
        {

            rb.useGravity = false;
        }
    }
}
