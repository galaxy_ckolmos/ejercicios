using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Propulsar : MonoBehaviour
{
    public void AñadirFuerza()
    {
        GetComponent<Rigidbody>().AddForce(Vector3.up * 4.5F, ForceMode.Impulse);
    }
}
