using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetComponentCamara : MonoBehaviour
{
    Camera camara;

    void Update()
    {
        bool teclaPulsada = Input.anyKey;

        if (teclaPulsada)
        {

            camara = GetComponent<Camera>();
            camara.clearFlags = CameraClearFlags.Color;
            camara.backgroundColor = Color.green;
        }
    }
}
