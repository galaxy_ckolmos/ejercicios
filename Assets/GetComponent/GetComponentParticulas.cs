using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetComponentParticulas : MonoBehaviour
{
    ParticleSystem particulas;

    private void Start()
    {
        gameObject.SetActive(false);
    }

    private void Update()
    {
        bool tecla = Input.anyKey;

        if (tecla)
        {
            particulas = GetComponent<ParticleSystem>();
            particulas.Play();
        }
    }
}
