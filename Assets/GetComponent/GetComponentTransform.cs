using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetComponentTransform : MonoBehaviour
{

    // No es necesario usar GetComponent<Transform> porque
    // MonoBehaviour ya tiene una variable transform
    // previamente asignada al transform del gameObject

    // Aqu� hay, sin embargo, un ejemplo de c�mo ser�a su uso

    Transform t;

    void Update()
    {

        t = GetComponent<Transform>();
        t.Translate(1, 0, 0);

        // Lo anterior es igual que:
        //transform.Translate(1, 0, 0);
    }

}
