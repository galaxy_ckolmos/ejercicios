using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetcomponentRigidbody : MonoBehaviour
{
    Rigidbody rb;
    BoxCollider collider;

    void Update()
    {
        bool teclaPulsada = Input.anyKey;

        if (teclaPulsada)
        {

            rb = GetComponent<Rigidbody>();
            rb.useGravity = true;

            collider = GetComponent<BoxCollider>();
            collider.enabled = false;
        }
    }
}
