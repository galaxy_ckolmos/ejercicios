using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotacionVariable : MonoBehaviour
{
    public bool traslaciónLocal = true;
    public float velocidadX = 0;
    public float velocidadY = 0;
    public float velocidadZ = 0;

    public bool rotaciónLocal = true;
    public float velocidadGiroX = 0;
    public float velocidadGiroY = 0;
    public float velocidadGiroZ = 0;

    void Update()
    {
        if (traslaciónLocal == true)
        {
            transform.Translate(velocidadX, velocidadY, velocidadZ);
        }
        else
        {
            transform.Translate(velocidadX, velocidadY, velocidadZ, Space.World);
        }

        if(rotaciónLocal == true)
        {
            transform.Rotate(velocidadGiroX, velocidadGiroY, velocidadGiroZ);
        }
        else
        {
            transform.Rotate(velocidadGiroX, velocidadGiroY, velocidadGiroZ, Space.World);
        }
        
    }
}
