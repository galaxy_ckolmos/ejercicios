using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EjemplosFunciones : MonoBehaviour
{

    private void Start()
    {
        /*
        int a = FuncionInt();
        print(a);

        FuncionConParametros(-3,4);

        ImprimeUnaFrase("Hola", "Mundo");
        ImprimeUnaFrase("Adios", "Colega");

        string p1 = "Palabra1";
        string p2 = "Palabra2";
        ImprimeUnaFrase(p1, p2);

        CambiarTimeScale(0.5F);

        bool verdadero_o_falso = ComprobarMayorQue(9, 7);
        print(verdadero_o_falso);
        */

        int a = 0;
        print(a);
        a = FuncionCompleta(5, -10);

        //print( FuncionCompleta(5, -10) );
        //print(FuncionSimple());

        print(a);
    }


    // EJEMPLOS B�SICOS
    void FuncionSimple()
    {
        print("Ni recibe ni devuelve nada");
    }

    int FuncionInt()
    {
        print("Devuelve un n�mero int");
        return 7;
    }

    void FuncionConParametros(int a, int b)
    {
        print("Recibe par�metros");
        int c = a + b;
        print(c);
    }

    int FuncionCompleta(int a, int b)
    {
        int c = a + b;
        return c;
    }
    //-



    // EJEMPLOS ESPECIFICOS
    int SumaNumerosAleatorios()
    {

        int a = Random.Range(0, 10);
        int b = Random.Range(0, 10);

        int c = a + b;
        return c;
    }

    void ImprimeUnaFrase(string palabra1, string palabra2)
    {
        string frase = palabra1 + " " + palabra2;
        print(frase);
    }

    void CambiarTimeScale(float velocidadTiempo)
    {
        Time.timeScale = velocidadTiempo;
    }

    bool ComprobarMayorQue(int primero, int segundo)
    {
        
        if (primero > segundo)
        {
            return true;
        }

        return false;
    }
}
