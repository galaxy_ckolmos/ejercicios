using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EjerciciosDiagramas1 : MonoBehaviour
{
    int A;
    int B;
    // Start is called before the first frame update
    void Start()
    {
        A = 5;
        print(A);
        B = 10;
        print(B);
    }

    // Update is called once per frame
    void Update()
    {
        A = ++A;
        B = --B;
        Start();
    }
}
