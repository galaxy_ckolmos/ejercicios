using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EjerciciosFunciones1 : MonoBehaviour
{
    int a = 2;
    int b = 5;
    int d = 7;

    // Start is called before the first frame update
    void Start()
    {
        miraLaFuncionAqui();
        vamoAMultiplica();
        funcionInutilDeLlamada();
    }

    // Update is called once per frame
    void Update()
    {
        miPeorPesadilla();
    }

    void miraLaFuncionAqui()
    {
        int c = a + b;
        print(c);
    }

    void vamoAMultiplica()
    {
        int e = a * b * d;
        print(e);

    }

    void miPeorPesadilla()
    {
        a = a + 1;
        print(a);

    }

    void funcionInutilDeLlamada()
    {
        print("Antes de la llamada");
        funcionQueSeraLlamada();
        print("Despu�s de la llamada");

    }

    void funcionQueSeraLlamada()
    {
        print("me han llamado :D");
    }

}
