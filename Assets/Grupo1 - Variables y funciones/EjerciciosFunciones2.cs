using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EjerciciosFunciones2 : MonoBehaviour
{

    void Start()
    {
        float resultado = Operacion(3f, 3f, 5f);
        float resultado2 = Operacion(-3f, 0f, 5f);

        ImprimeMundo("Hola");
    }

    float Operacion(float a, float b, float c)
    {
        return (a + b) * c;
    }

    string Hola()
    {
        return "Hola";
    }

    void ImprimeHola()
    {
        print("Hola");
    }

    void ImprimeMundo(string palabra)
    {
        print("Mundo");
    }
}
