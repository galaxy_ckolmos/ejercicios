using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Matematicas : MonoBehaviour
{
    int SumarEnteros(int a, int b)
    {
        return a + b;
    }

    float SumarFloats(float a, float b)
    {
        return a + b;
    }

    float TransformarIntFloat(int a)
    {
        float b = a;
        return b;
    }

    int RedondeoFloatInt(float a)
    {
        int resultado = Mathf.RoundToInt(a);
        return resultado;
    }

    int RedondeoSueloFloat(float a)
    {
        int resultado = Mathf.FloorToInt(a);
        return resultado;
    }

    string StringficarInt(int a)
    {
        string resultado = a.ToString();
        return resultado;
    }


}
