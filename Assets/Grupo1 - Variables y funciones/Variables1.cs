using UnityEngine;

public class Variables1 : MonoBehaviour
{
    // VARIABLES B�SICAS
    int numeroEntero = 2;        //N�mero entero
    float numeroDecimal = 4.5F;  //N�mero decimal
    bool valorL�gico = false;    //Valor l�gico
    string texto = "Hola Mundo"; //Cadena de texto

    private void Start()
    {

        int a; // Creaci�n de una variable (DECLARACI�N)
        a = 5; // ASIGNACI�N, INICIALIZACI�N

        int variable1 = 5;
        int variable2 = -5;
        int variable3 = 10;

        int resultado1 = variable1 + variable2;
        print(resultado1);

        int resultado2 = variable2 * variable3;
        print(resultado2);

        int resultado3 = resultado1 - resultado2;
        //�Qu� podemos poner en esta l�nea para ver en la consola resultado3?

    }
}
