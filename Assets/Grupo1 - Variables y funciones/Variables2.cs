using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Variables2 : MonoBehaviour
{
    int variableMiembro = 10; // Variable miembro de la clase. Se puede usar en todo el script

    int a = 1;
    //int a = 1; // Ya existe una variable a. Error.

    void Start()
    {
        int variableStart = 1; // Variable local. Solo se puede usar en Start

        print(variableMiembro);
        print(variableStart);
        //print(variableUpdate); // Dar�a error porque no existe en el contexto (en Start)
    }

    void Update()
    {
        int variableUpdate = -1; // Variable local. Solo se puede usar en Update
        print(variableMiembro);
        //print(variableStart); // Dar�a error porque no existe en el contexto (en Update)
        print(variableUpdate);
    }
}