using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class E10_Boton : MonoBehaviour
{
    public Image imagen;
    public Text texto;

    bool contento = false;

    public void BotonPulsado()
    {
        if (!contento)
        {
            contento = true;
            imagen.color = Color.green;
            texto.text = ":D";
        }
        else
        {
            contento = false;
            imagen.color = Color.red;
            texto.text = ":(";
        }

    }
}
