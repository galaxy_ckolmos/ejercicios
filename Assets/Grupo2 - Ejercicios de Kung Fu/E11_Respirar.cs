using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E11_Respirar : MonoBehaviour
{

    public float velocidadRespiracion = 2;
    public float amplitud = 2;

    Vector3 escalaInicial;

    bool creciendo = true;



    private void Start()
    {
        escalaInicial = transform.localScale;
    }

    private void Update()
    {
        //Metodo1();
        Metodo2();

    }

    void Metodo1()
    {
        float escala = (Mathf.Sin(Time.time * velocidadRespiracion) * amplitud) + escalaInicial.x;
        transform.localScale = new Vector3(escala, escala, escala);
    }

    void Metodo2()
    {
        float escalaMax = escalaInicial.x + 3;
        float escalaMin= escalaInicial.x - 3;


        if (creciendo)
        {
            if (transform.localScale.x < escalaMax)
            {
                transform.localScale += Vector3.one * Time.deltaTime * velocidadRespiracion;
            }
            else
            {
                creciendo = false;
            }
        }
        else
        {
            if (transform.localScale.x > escalaMin)
            {
                transform.localScale -= Vector3.one * Time.deltaTime* velocidadRespiracion;
            }
            else
            {
                creciendo = true;
            }
        }
    } 
}
