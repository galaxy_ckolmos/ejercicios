using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E12_CambioColor : MonoBehaviour
{
    int contactos = 0;

    private void Start()
    {
        GetComponent<Renderer>().material.color = Color.green;
    }

    private void OnCollisionEnter(Collision collision)
    {
        contactos++;
        GetComponent<Renderer>().material.color = Color.red;
    }

    private void OnCollisionStay(Collision collision)
    {
        GetComponent<Renderer>().material.color = Color.blue;
    }

    private void OnCollisionExit(Collision collision)
    {
        contactos--;
        if (contactos == 0)
        {
            GetComponent<Renderer>().material.color = Color.green;
        }
    }
}
