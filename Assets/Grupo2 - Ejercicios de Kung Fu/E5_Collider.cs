using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E5_Collider : MonoBehaviour
{
    Collider collider;

    private void Start()
    {
        collider = GetComponent<Collider>();
    }

    void Update()
    {
        if (transform.position.z >= 10 && collider.enabled == true)
        {
            collider.enabled = false;
        }
    }
}
