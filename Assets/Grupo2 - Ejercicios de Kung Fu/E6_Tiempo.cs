using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class E6_Tiempo : MonoBehaviour
{
    public Light luz;

    int pulsaciones = 0;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            print(Time.time);

            pulsaciones++;

            if (pulsaciones == 3)
            {
                luz.color = new Color(Random.Range(0F,1f), Random.Range(0F, 1f), Random.Range(0F, 1f));
            }
        }

        if (Time.time > 10)
        {
            SceneManager.LoadScene(0);
        }
    }
}
