using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E7_Materiales : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        Renderer renderer1 = GetComponent<Renderer>();
        Renderer renderer2 = collision.gameObject.GetComponent<Renderer>();

        Material rendererAux = renderer1.material;

        renderer1.material = renderer2.material;
        renderer2.material = rendererAux;
    }
}
