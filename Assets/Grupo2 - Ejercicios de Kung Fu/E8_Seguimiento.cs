using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E8_Seguimiento : MonoBehaviour
{
    public GameObject objetivo;
    public float velocidad=10;

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position,objetivo.transform.position,Time.deltaTime* velocidad);
    }
}
