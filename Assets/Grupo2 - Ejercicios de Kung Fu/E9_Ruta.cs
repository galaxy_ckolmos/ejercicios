using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E9_Ruta : MonoBehaviour
{
    public Transform ruta1;
    public Transform ruta2;
    public Transform ruta3;
    public Transform ruta4;

    Transform siguiente;
    int siguientePunto = 1;

    private void Start()
    {
        siguiente = ruta1;
    }

    private void Update()
    {
        //Version1();
        Version2();
    }

    void Version1()
    {
        transform.position = Vector3.Lerp(transform.position, siguiente.position, Time.deltaTime);
        ComprobarPunto();
    }

    void Version2()
    {
        transform.LookAt(siguiente);
        transform.Translate(Vector3.forward * Time.deltaTime * 10);
        ComprobarPunto();

    }

    void ComprobarPunto()
    {
        if (Vector3.Distance(transform.position, siguiente.position) < 0.1F)
        {
            transform.position = siguiente.position;

            switch (siguientePunto)
            {
                case 1:
                    siguiente = ruta2;
                    break;
                case 2:
                    siguiente = ruta3;
                    break;
                case 3:
                    siguiente = ruta4;
                    break;
                case 4:
                    siguiente = ruta1;
                    break;
            }

            siguientePunto++;
            if (siguientePunto > 4) { siguientePunto = 0; }

        }
    }
}
