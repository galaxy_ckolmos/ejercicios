using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ej1_Movimiento : MonoBehaviour
{
    public float velocidad = 10;

    void Update()
    {

        float x = Input.GetAxis("Horizontal")*velocidad*Time.deltaTime;
        float y = Input.GetAxis("Vertical") * velocidad*Time.deltaTime;

        transform.Translate(x,0,y);   
    }
}
