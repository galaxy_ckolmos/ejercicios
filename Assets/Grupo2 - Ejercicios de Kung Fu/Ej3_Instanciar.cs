using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ej3_Instanciar : MonoBehaviour
{
    public GameObject prefab;

    void Update()
    {
        bool raton = Input.GetMouseButtonDown(0);
        //bool raton = Input.GetKey(KeyCode.Mouse0);
        if (raton)
        {
            Instantiate(prefab, new Vector3(0, 10, 0), Quaternion.identity);
        }
    }
}
