using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoSinuidal : MonoBehaviour
{
    [Range(1,10)]
    public float amplitud = 1;

    void Update()
    {
        if (transform.position.x < 5)
        {
            transform.position = new Vector3(Mathf.Sin(Time.time) * amplitud, transform.position.y, transform.position.z);
        }
    }

    private void OnMouseDown()
    {
        Destroy(gameObject);
    }
}
