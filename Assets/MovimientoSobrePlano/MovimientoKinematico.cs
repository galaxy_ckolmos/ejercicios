using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoKinematico : MonoBehaviour
{
    enum ModoMovimiento
    {
        KINEMATICO, 
        FISICAS
    }
    
    public float velocidad = 2;
    Vector3 direction;
    void FixedUpdate()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        direction = new Vector3(x,0,z);

        if(!Physics.BoxCast(transform.position, transform.localScale/2, direction,Quaternion.identity,0.5F))
        {
            transform.Translate(x * velocidad * Time.deltaTime, 0, z * velocidad * Time.deltaTime);
        }

    }

    void OnDrawGizmos()
    {
        // Draw a semitransparent blue cube at the transforms position
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawCube(transform.position+ direction, new Vector3(1, 1, 1));
    }
}
