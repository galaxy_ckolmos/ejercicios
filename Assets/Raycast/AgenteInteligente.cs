using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgenteInteligente : MonoBehaviour
{
    public NavMeshAgent agente;
    


    void Update()
    {
        ClickRaycast();
    }

    void ClickRaycast()
    {
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;

            Ray rayo = Camera.main.ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(rayo.origin, rayo.direction * 100, Color.blue);
            bool choque = Physics.Raycast(rayo, out hit, 100);
            if (choque)
            {
                agente.SetDestination(hit.point);
            }
        }
    }
}
