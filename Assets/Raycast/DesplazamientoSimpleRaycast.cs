using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesplazamientoSimpleRaycast : MonoBehaviour
{
    public float velocidad = 5;
    public float distanciaDeteccion = 2;

    void Update()
    {

        RaycastHit hit;

        DibujarRayos();

        float x = Input.GetAxis("Horizontal") * Time.deltaTime * velocidad;
        float z = Input.GetAxis("Vertical"  ) * Time.deltaTime * velocidad;

        bool derecha   = Physics.Raycast(transform.position,Vector3.right  ,out hit, distanciaDeteccion);
        bool izquierda = Physics.Raycast(transform.position, -Vector3.right, out hit, distanciaDeteccion);
        bool arriba    = Physics.Raycast(transform.position, Vector3.forward    , out hit, distanciaDeteccion);
        bool abajo     = Physics.Raycast(transform.position, -Vector3.forward   , out hit, distanciaDeteccion);

        bool comprobacionDerecha   = x > 0 && !derecha;
        bool comprobacionIzquierda = x < 0 && !izquierda;
        bool comprobacionArriba    = z > 0 && !arriba;
        bool comprobacionAbajo     = z < 0 && !abajo;


        if (comprobacionDerecha || comprobacionIzquierda || comprobacionArriba || comprobacionAbajo) {
            transform.Translate(x, 0, z);
        }

    }

    void DibujarRayos()
    {
        Debug.DrawRay(transform.position, Vector3.right * distanciaDeteccion, Color.red);
        Debug.DrawRay(transform.position, -Vector3.right * distanciaDeteccion, Color.red);
        Debug.DrawRay(transform.position, Vector3.forward * distanciaDeteccion, Color.red);
        Debug.DrawRay(transform.position, -Vector3.forward * distanciaDeteccion, Color.red);
    }
}
