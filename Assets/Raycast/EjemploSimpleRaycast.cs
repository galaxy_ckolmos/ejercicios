using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EjemploSimpleRaycast : MonoBehaviour
{

    public float distancia = 2;  // C�mo de lejos va a llegar el rayo

    void Update()
    {
        RaycastHit hit; // Guarda la informaci�n del choque
        
        // Creamos un rayo
        Vector3 origen = transform.position; // Posici�n de origen del rayo
        Vector3 direccion = Vector3.forward; // Hacia d�nde lanzar el rayo
        Ray rayo = new Ray(origen, direccion); 

        // Lanzamos el rayo
        // Si el rayo choca, Raycast devuelve TRUE y se rellena la variable hit.
        // Si el rayo no choca, Raycast devuelve FALSE y NO se rellena la variable hit (que sigue siendo null)
        bool impacto = Physics.Raycast(rayo, out hit, distancia);

        //DRAWLINE NO ES UN RAYCAST, pero ayuda a visualizarlo si se hace bien
        Debug.DrawRay(origen,direccion*distancia,Color.red);
       

        if (impacto == true)
        {
            print("Choque contra: "+hit.collider);
        }
        else
        {
            transform.Translate(0, 0, 2 * Time.deltaTime);
        }
    }

}
