using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EjemplosRaycast : MonoBehaviour
{

    void Update()
    {

        //EjemploOrigenDireccion();

        EjemploRayo();
        
    }

    void EjemploRayo()
    {
        RaycastHit hit;
        Vector3 origen = transform.position;
        Vector3 direccion = Vector3.forward;
        Ray rayo = new Ray(origen, direccion);
        float distancia = 2;
        bool impacto = Physics.Raycast(rayo,out hit, distancia, LayerMask.GetMask("obstaculo"));
        if (impacto == true)
        {
            print(LayerMask.GetMask("obstaculo"));
            print(hit.collider.name);
        }
        else
        {
            transform.Translate(0, 0, 2 * Time.deltaTime);
        }
    }

    void EjemploOrigenDireccion()
    {

        Vector3 origen = transform.position;
        Vector3 direccion = Vector3.forward;
        float distancia = 2;


        RaycastHit hit;

        Debug.DrawRay(origen, direccion * distancia, Color.green);
        bool impacto = Physics.Raycast(origen, direccion, out hit, distancia, LayerMask.GetMask("obstaculo"));

        if (impacto == true)
        {
            print(LayerMask.GetMask("obstaculo"));
            print(hit.collider.name);
        }
        else
        {
            transform.Translate(0, 0, 2 * Time.deltaTime);
        }
    }

}
