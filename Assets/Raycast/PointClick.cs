using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointClick : MonoBehaviour
{
    //bool desplazarse = false;
    Vector3 destino;

    private void Start()
    {
        destino = transform.position;
    }

    void Update()
    {
        ClickRaycast();
        Desplazarse();
    }

    void ClickRaycast()
    {
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;

            Ray rayo = Camera.main.ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(rayo.origin, rayo.direction * 50, Color.blue);
            bool choque = Physics.Raycast(rayo, out hit, 50);
            if (choque)
            {
                destino = hit.point;
            }
        }
    }

    void Desplazarse()
    {
        if( Vector3.Distance(transform.position,destino)>0.001F  )
        {
            Vector3 posicion = transform.position;
            Vector3 moveTowards = Vector3.MoveTowards(transform.position, destino, 5 * Time.deltaTime);

            transform.position = new Vector3(moveTowards.x, posicion.y, moveTowards.z);
        }
        
    }
}
