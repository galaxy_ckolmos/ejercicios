using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Snake : MonoBehaviour
{
    int tama�o=5;
    Vector3 posicion = Vector3.zero;
    Vector3 posicionFruta = Vector3.zero;
    List<GameObject> cuerpo = new List<GameObject>();
    GameObject fruta;
    bool frutaColocada;
    bool existeFruta = false;
    bool move = true;

    enum Direccion
    {
        ARRIBA,
        ABAJO,
        IZQUIERDA,
        DERECHA
    }

    Direccion direccion = Direccion.DERECHA;

    private void Start()
    {
        GenerarEscenario();
        StartCoroutine(Mover());
        StartCoroutine(Fruta());
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow) && direccion != Direccion.IZQUIERDA) direccion = Direccion.DERECHA;
        if (Input.GetKeyDown(KeyCode.LeftArrow)  && direccion != Direccion.DERECHA)   direccion = Direccion.IZQUIERDA;
        if (Input.GetKeyDown(KeyCode.UpArrow)    && direccion != Direccion.ABAJO)     direccion = Direccion.ARRIBA;
        if (Input.GetKeyDown(KeyCode.DownArrow)  && direccion != Direccion.ARRIBA)    direccion = Direccion.ABAJO;
    }

    void GenerarEscenario()
    {
        for(int i = 0; i < 17; i++)
        {
            GameObject parte = GameObject.CreatePrimitive(PrimitiveType.Cube);
            parte.transform.position= new Vector3(i-8f,0,5);
            GameObject parte2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            parte2.transform.position = new Vector3(i - 8f, 0, -5);
        }

        for (int i = 0; i < 9; i++)
        {
            GameObject parte = GameObject.CreatePrimitive(PrimitiveType.Cube);
            parte.transform.position = new Vector3(8f, 0, i-4F);
            GameObject parte2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            parte2.transform.position = new Vector3(-8f, 0, i-4f);
        }
    }

    IEnumerator Mover()
    {
        while (move)
        {
            for (int i=0; i < tama�o; i++){
                bool comprobaci�n = ControlarDireccion();
                bool vivo = true;
                if (comprobaci�n)
                {
                     vivo = ComprobarVida();
                }
                if (vivo)
                {
                    GameObject parte = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    parte.transform.position = posicion;
                    if (posicion == posicionFruta)
                    {
                        tama�o++;
                        Destroy(fruta);
                        existeFruta = false;
                    }
                    cuerpo.Add(parte);
                    EliminarPartes();
                    yield return new WaitForSeconds(0.2F);
                }
                else
                {
                    move = false;
                    break;
                }
            }  
        }
    }

    IEnumerator Fruta()
    {
        while (true)
        {
            //posicionFruta = Vector3.zero;
            while (!frutaColocada && !existeFruta)
            {
                posicionFruta = new Vector3(Random.Range(-8, 9), 0, Random.Range(-4, 4));
                frutaColocada = true;
                for (int i = 0; i < cuerpo.Count; i++)
                {
                    if (cuerpo[i].transform.position == posicionFruta)
                    {
                        frutaColocada = false;
                        break;
                    }
                }
            }
            fruta = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            fruta.transform.position = posicionFruta;
            existeFruta = true;

            yield return new WaitForSeconds(2);
        }
    }

    bool ControlarDireccion()
    {
        switch (direccion)
        {
            case Direccion.DERECHA:
                posicion.x++;
                break;
            case Direccion.IZQUIERDA:
                posicion.x--;
                break;
            case Direccion.ARRIBA:
                posicion.z++;
                break;
            case Direccion.ABAJO:
                posicion.z--;
                break;
        }
        if(posicion.x==9 || posicion.x==-9 || posicion.z == 5 || posicion.z == -5)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            return false;
        }
        return true;
    }

    bool ComprobarVida()
    {
        for(int i=0;i<cuerpo.Count;i++)
        {
            for(int j= 0; j < cuerpo.Count; j++)
            {
                if (i != j)
                {
                    if (cuerpo[i].transform.position == cuerpo[j].transform.position)
                    {
                        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                        return false;
                    }
                }
            }
        }

        return true;
    }

    void EliminarPartes()
    {
        if (cuerpo.Count > tama�o)
        {
            GameObject destruir = cuerpo[0];
            cuerpo.RemoveAt(0);
            Destroy(destruir);
        }
    }
}
